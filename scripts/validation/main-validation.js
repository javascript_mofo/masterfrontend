//===========================================================
//      VALIDATION
//===========================================================
define(['jquery',
        'jquery-ui',
        'jquery.validate',
        'jquery.steps'],
        function($){

        /* steps form show numbers on mobile */
        var hideNumeric = function(el){
            var getText = el;
            getText.html('');
            getText.each(function(i){
                var num = i + 1;
                $(this).prepend($('<span/>', {
                    'text' : num
                }));
            });
        };
// these 2 functions can be turned into methods somewhere
    var highlightRules = function(element){
        var id_attr = "#" + $( element ).attr("id") + "1";
        $(element).closest('.form-control').removeClass('has-success').addClass('has-error');
        $(id_attr).removeClass('glyphicon-ok').addClass('glyphicon-remove');
    };

    var unHighlightRules = function(element){
        var id_attr = "#" + $( element ).attr("id") + "1";
        $(element).closest('.form-control').removeClass('has-error').addClass('has-success');
        $(id_attr).removeClass('glyphicon-remove').addClass('glyphicon-ok');
    };
// Validation Reg Ex
$.validator.addMethod("postcodeUK", function(value, element) {
    return this.optional(element) || /^([Gg][Ii][Rr] 0[Aa]{2})|((([A-Za-z][0-9]{1,2})|(([A-Za-z][A-Ha-hJ-Yj-y][0-9]{1,2})|(([A-Za-z][‌​0-9][A-Za-z])|([A-Za-z][A-Ha-hJ-Yj-y][0-9]?[A-Za-z])))) {0,1}[0-9][A-Za-z]{2})$/i.test(value);
}, "Please enter a VALID Postcode");

$.validator.addMethod("phoneUK", function(value, element) {
    return this.optional(element) || /^(0044|0|\+?44)[12378]\d{8,9}$/.test(value);
}, "Please enter a VALID Phone Number");

$.validator.addMethod("mobileUK", function(value, element) {
    return this.optional(element) || /^(0044|0|\+?44)[12378]\d{9}$/.test(value);
}, "Please enter a VALID Phone Number");

// Funeral Validation
    require(['classes/MainPreloader',
        'classes/AddHiddenClass',
        'classes/ConcatenateDobSubClass',
        'classes/ScrollToConnectedLink'],
        function(MainPreloader,
                 AddHiddenClass,
                 ConcatenateDobSubClass,
                 ScrollToConnectedLink) {

            // Funeral Validation


            var oneStageForm = new MainPreloader("viewOptionBtn", "hideForm", "showPreloader", 1000, "mainForm");
            //scroll back to top after for submit
             var scrollTopOnSubmit = new ScrollToConnectedLink("viewOptionBtn","topOfPage", 1000, 'easeOutExpo');
            scrollTopOnSubmit.scrollUpAfterPre();



            $('#mainForm').validate({
                // Rules
                rules: {
                    fname: {
                        required: true
                    },
                    lname: {
                        minlength: 2,
                        required: true
                    },
                    main_phone: {
                        required: true,
                        phoneUK: true
                    },

                    second_phone: {

                        mobileUK: true
                    },
                    /*  dd_dob_day: {
                     required: true
                     },
                     dd_dob_month: {
                     required: true
                     },
                     dd_dob_year: {
                     required: true
                     },*/
                    email: {
                        required: true,
                        email: true
                    }
                },
                // CSS Rules
                highlight: function(element) {
                    var id_attr = "#" + $( element ).attr("id") + "1";
                    $(element).closest('.form-control').removeClass('has-success').addClass('has-error');
                    $(id_attr).removeClass('glyphicon-ok').addClass('glyphicon-remove');
                },
                unhighlight: function(element) {
                    var id_attr = "#" + $( element ).attr("id") + "1";
                    $(element).closest('.form-control').removeClass('has-error').addClass('has-success');
                    $(id_attr).removeClass('glyphicon-remove').addClass('glyphicon-ok');
                },
                errorElement: 'span',
                errorClass: 'help-block',
                errorPlacement: function(error, element) {
                    if(element.length) {
                        error.insertAfter(element);
                    } else {
                        error.insertAfter(element);
                    }
                },
                submitHandler: function(){



                    // Tranverse the D.O.B inputs
                    var $day = $("#dd_dob_day").val();
                    var $month = $("#dd_dob_month").val();
                    var $year = $("#dd_dob_year").val();
                    var $dateOfBirth = $day + " " + $month + " " + $year;


                    // append DOB inputs
                    var input = $("<input>")
                        .attr("type" , "hidden")
                        .attr("name", "main_dob").val($dateOfBirth);
                    $("#mainForm").append($(input));


                    // var testForm = $("#mainForm").serialize();

                    // alert(testForm);

                    submitLead("http://ws.xhtransfer.co.uk/funeralplans.aspx", "mainForm");

                   oneStageForm.oneStageSendPreLoader();
                }

            });

        }); // end of Require block
//================================================================================
// Steps Form (Life)
//================================================================================

// #stepsFormDebt
// Pre Form Questions for Steps Form
    require(['classes/MainPreloader', 'classes/AddHiddenClass', 'classes/ConcatenateDobSubClass'], function(MainPreloader, AddHiddenClass,ConcatenateDobSubClass){

            var lifeStepsForm = new MainPreloader("lifeStepsPre", "preformFormsteps" ,"showPreloader", 3000, "hideStepsForm");
            var lifeStepsFormSubmit = new MainPreloader("lifeStepsPre", "hideStepsForm" ,"showPreloader", 4000, "hideStepsForm");

            // hide the preform on click
            //  lifeStepsPre
            var preForm  =  $('#stepsPreForm');

            preForm.on('submit', function(e){
                var isvalidate = preForm.valid();
                if(isvalidate)
                {
                    // before loading the form add the patner details if needed:
                    $cover_forVal = $('[name=cover_for]').val();
                    $cover_termVal = $('[name=cover_term]').val();

                    if($('[name=cover_for]').val() === "single"){

                        e.preventDefault();
                        // gets values
                        lifeStepsForm.getPreFormValues(preForm);


                    }else if($('[name=cover_for]').val() === "double"){
                        e.preventDefault();



                        $("#stepsFormLife").steps("insert", 2, {
                            title: "Your Partner",
                            content: $("#partnerDetialsHidden").html()
                        });
                        //============================================
                        // function to hide text on steps
                        // hide text on page load if ipad etc.
                        if($(window).width() < 768){
                            hideNumeric($(".steps ul li a"));
                        }
                        // hide text on resize
                        $(window).resize(function(){
                            if($(window).width() < 768){
                                hideNumeric($(".steps ul li a"));
                            }
                        });
                        //===============================================

                        // gets values
                        lifeStepsForm.getPreFormValues(preForm);
                    }
                }
            }); // pre Form

                var  stepsFormLife =  $('#stepsFormLife');
                stepsFormLife.validate({
                    // Rules
                    rules: {
                        fname: {
                            required: true
                        },
                        lname: {
                            minlength: 2,
                            required: true
                        },
                        partner_fname: {
                            required: true
                        },
                        partner_lname: {
                            minlength: 2,
                            required: true
                        },
                        main_phone: {
                            required: true,
                            phoneUK: true
                        },
                        second_phone: {
                            mobileUK: true
                        },
                        dd_dob_day: {
                            required: true
                        },
                        dd_dob_month: {
                            required: true
                        },
                        dd_dob_year: {
                            required: true
                        },
                        txt_house_name_no: {
                            required: true
                        },
                        post_code: {
                            required: true,
                            postcodeUK: true
                        },
                        email: {
                            required: true,
                            email: true
                        },
                        house_number: {
                            required: true
                        }
                    },

                    // CSS Rules
                    highlight: function(element) {
                        highlightRules(element);
                    },
                    unhighlight: function(element) {
                        unHighlightRules(element);
                    },
                    errorElement: 'span',
                    errorClass: 'help-block',
                    errorPlacement: function(error, element) {
                        if(element.length) {
                            error.insertAfter(element);
                        } else {
                            error.insertAfter(element);
                        }
                    },
                    submitHandler: function(){
                        // sendToThankYouPage();
                    }
                });

                // Steps Module Set-up
                //steps

                if(stepsFormLife.length) {
                    // Form Steps
                    stepsFormLife.steps(
                        {
                            headerTag: "h3",
                            bodyTag: "section",
                            transitionEffect: "fade",
                            autoFocus: true,

                            labels: {
                                finish: "Submit"
                            },
                            onStepChanging: function (event, currentIndex, newIndex)
                            {
                                if(currentIndex > newIndex){
                                    return true;
                                }
                                stepsFormLife.validate().settings.ignore = ":disabled,:hidden";
                                return  stepsFormLife.valid();
                            },
                            onFinishing: function (event, currentIndex)
                            {
                                stepsFormLife.validate().settings.ignore = ":disabled";
                                return  stepsFormLife.valid();
                            },
                            onFinished: function (event, currentIndex)
                            {
                               var concatDobDebt = new ConcatenateDobSubClass($("#dd_dob_day"), $("#dd_dob_month"), $("#dd_dob_year"), "main_dob", "stepsFormLife");
                                concatDobDebt.concatDob();

                                if($cover_forVal === "double"){

                                    var concatDobDebtPart = new ConcatenateDobSubClass($("#dd_ptnr_dob_day"), $("#dd_ptnr_dob_month"), $("#dd_ptnr_dob_year"), "partner_dob", "stepsFormLife");
                                    concatDobDebtPart.concatDob();
                                }else if ($cover_forVal === "single"){
                                    var $dateOfBirthPart = "01 JAN 1995";
                                    var $cover_termValFoo = new AddHiddenClass($dateOfBirthPart,$dateOfBirthPart, "partner_dob", "stepsFormLife");
                                    $cover_termValFoo.createHiddenFields();
                                }

                                // hidden fields
                                var $cover_type = "1";
                                var $cover_typeFoo = new AddHiddenClass($cover_type, $cover_type, "cover_type", "stepsFormLife");
                                $cover_typeFoo.createHiddenFields();

                                var $cover_forValFoo = new AddHiddenClass($cover_forVal, $cover_forVal, "cover_for", "stepsFormLife");
                                $cover_forValFoo.createHiddenFields();

                                var $cover_termValFoo = new AddHiddenClass($cover_termVal, $cover_termVal, "cover_term", "stepsFormLife");
                                $cover_termValFoo.createHiddenFields();

                                var $with_cic = "false";
                                var $with_cicFoo = new AddHiddenClass($with_cic, $with_cic, "with_cic", "stepsFormLife");
                                $with_cicFoo.createHiddenFields();

                               var $premium_type = "1";
                                var $premium_typeFoo = new AddHiddenClass( $premium_type, $premium_type, "premium_type", "stepsFormLife");
                                $premium_typeFoo.createHiddenFields();

                                var $partner_smoker = $('input[name=partner_smoker]:checked', '#stepsFormLife').val();
                                if($partner_smoker == "No" || $partner_smoker == "Yes"){

                                    var $partner_smokerFoo = new AddHiddenClass($partner_smoker, $partner_smoker, "partner_smoker", "stepsFormLife");
                                    $partner_smokerFoo.createHiddenFields();


                                } else {
                                    var $partner_smoker3 = "";
                                    var $partner_smoker3Foo = new AddHiddenClass($partner_smoker3, null, "partner_smoker", "stepsFormLife");
                                    $partner_smoker3Foo.createHiddenFields();

                                }

                                var $partner_fname = $("#partner_fname").val();
                                if($partner_fname == "" || $partner_fname == undefined) {
                                    var $partnerlnameFoo = new AddHiddenClass($partner_fname, null, "partner_fname", "stepsFormLife");
                                    $partnerlnameFoo.createHiddenFields();
                                }

                                var $partner_lname = $("#partner_lname").val();
                                if($partner_lname == "" || $partner_lname == undefined){

                                    var $partnerlnameFoo = new AddHiddenClass($partner_lname , null, "partner_lname", "stepsFormLife");
                                        $partnerlnameFoo.createHiddenFields();
                                }

                                // Partner Title Field (not in HTML)
                                if($partner_lname == "" || $partner_lname == undefined){

                                   var $partner_titleFoo = new AddHiddenClass($partner_lname, null, "partner_title", "stepsFormLife");
                                       $partner_titleFoo.createHiddenFields();
                                }

                                var $gender = "";
                                var $genderFoo = new AddHiddenClass( $gender, "", "gender", "stepsFormLife");
                                $genderFoo.createHiddenFields();

                                var testForm = stepsFormLife.serialize();

                              //  alert(testForm);
                              submitLead("http://ws.xhtransfer.co.uk/internalleadwithid.aspx", "stepsFormLife");

                               // lifeStepsFormSubmit.sendTothankYouPage();
                            }

                        });
                }
    }); // end of Require


//================================================================================
// Debt Form Validation
//================================================================================
        $('#debtForm').validate({
            // Rules
            rules: {
                fname: {
                    required: true
                },
                lname: {
                    required: true
                },
                house_number: {
                    maxlength: 15,
                    required: true
                },
                post_code: {
                    required: true,
                    postcodeUK: true
                },
                main_phone: {
                    required: true,
                    phoneUK: true
                },

                second_phone: {
                    required: true,
                    mobileUK: true
                },
                email: {
                    required: true,
                    email: true
                }
            },
            // CSS Rules
            highlight: function(element) {
                var id_attr = "#" + $( element ).attr("id") + "1";
                $(element).closest('.form-control').removeClass('has-success').addClass('has-error');
                $(id_attr).removeClass('glyphicon-ok').addClass('glyphicon-remove');
            },
            unhighlight: function(element) {
                var id_attr = "#" + $( element ).attr("id") + "1";
                $(element).closest('.form-control').removeClass('has-error').addClass('has-success');
                $(id_attr).removeClass('glyphicon-remove').addClass('glyphicon-ok');
            },


            errorElement: 'span',
            errorClass: 'help-block',
            errorPlacement: function(error, element) {
                if(element.length) {
                    error.insertAfter(element);
                } else {
                    error.insertAfter(element);
                }
            },
            submitHandler: function(){



                //  var testForm = $("#debtForm").serialize();

                // alert(testForm);
                submitLead("http://ws.xhtransfer.co.uk/debtmng.aspx","debtForm");
               // sendToThankYouPage();

            }

        });
//=============== Thank you Page ====================
$('#thankYouMsg').addClass('hatch');
//===================================================
//======================Form ========================
// Main Ajax Call to Web Service
        function submitLead(formAddr, formType) {

            var urlTest = formAddr;
            var thisId = formType;
			var postURLFromContent = $("#post_URL").val();
			
			

            $.ajax({
                url: postURLFromContent,
                type: "POST",
                data: $('#' + thisId).serialize(),
                cache: false,
                beforeSend: function () {

                },

                success: function (data) {

                    //alert('SUCCESS');

                    if(isNumeric(data))
                    {
                        //alert('Redirect to thankyou with qs values for tracking pixel');

                        window.location.replace("thankyou?s=1&ch=" + (loadPageVar("ch")) + "&lid=" + data);
                    }
                    else
                    {
                        window.location.replace("thankyou?s=0");
                    }


                },

                error: function (data) {

                    //alert('ERROR');
                }
            });
        }
function loadPageVar(sVar) { return unescape(window.location.search.replace(new RegExp("^(?:.*[&\\?]" + escape(sVar).replace(/[\.\+\*]/g, "\\$&") + "(?:\\=([^&]*))?)?.*$", "i"), "$1"));}
function isNumeric(num) {
    return !isNaN(num)
}

$("#lifeStepsPre").on("click", function(){
    //============================================
    // function to hide text on steps
    // hide text on page load if ipad etc.
    if($(window).width() < 768){
        hideNumeric($(".steps ul li a"));
    }
    // hide text on resize
    $(window).resize(function(){
        if($(window).width() < 768){
            hideNumeric($(".steps ul li a"));
        }
    });
    //===============================================
});


}); // require
