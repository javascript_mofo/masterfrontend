/**
 * Created by JGasewicz on 24/06/2015.
 */
require.config({
   // baseUrl: 'scripts/mstest/scripts',
    baseUrl: 'scripts/ms1Require/scripts',
    // baseUrl: 'scripts',
    paths: {
        'jquery' : 'lib/jquery',
        'bootstrap' : 'lib/bootstrap',
        'jquery-ui' : 'lib/jquery-ui',
        'jquery.validate' : 'lib/jquery.validate',
        'jquery.steps' : 'lib/jquery.steps'
    },

    shim: {
        'bootstrap' : {
            deps : ['jquery']
        },
        'jquery-ui' : {
            deps :  ['jquery']
        },
        'jquery.validate' : {
            deps : ['jquery']
        },
        'jquery.steps' : {
            deps : ['jquery']
        }
    }
});
