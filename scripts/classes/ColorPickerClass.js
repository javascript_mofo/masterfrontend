define(['jquery'], function($){
    // Super Class
    function ColorPickerClass()
    {

    }

// Super Class Methods
    ColorPickerClass.prototype =
    {
        swapColorsOnHover : function()
        {
            var mainClass = $('.' + this.classOfBtn);
            var _this = this;
            var $this = $(this);

            var mainBtnColor = mainClass.css('background-color');
            var fontBtnColor = mainClass.css('color');


            $('.' + _this.classOfBtn).hover(function(){
                    $('.' + _this.classOfBtn).css("color", mainBtnColor);
                    $('.' + _this.classOfBtn).css("border-color", mainBtnColor);
                    $('.' + _this.classOfBtn).css("background-color", "transparent");

                },
                function()
                {
                    var $this = $(this);
                    $this.css('color',fontBtnColor );
                    $this.css('border-color', mainBtnColor );
                    $this.css('background-color',mainBtnColor);
                }

            );

        }


    };

    return ColorPickerClass;
});
