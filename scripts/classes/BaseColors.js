/**
 * Created by JGasewicz on 25/06/2015.
 */
define(['classes/ColorPickerClass', '../util/extendObj'], function(ColorPickerClass, extendObj){


// Sub Class # 1
var BaseColors = function(cla)
{
    this.classOfBtn = cla;
};
extendObj(BaseColors, ColorPickerClass);

    return BaseColors;

});

