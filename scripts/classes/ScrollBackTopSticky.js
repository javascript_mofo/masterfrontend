/**
 * Created by JGasewicz on 25/06/2015.
 */
define(['classes/ScrollToLinkClass', '../util/extendObj'], function(ScrollToLinkClass, extendObj){


// sub class #2
var ScrollBackTopSticky = function(height)
{
    this.height = height;
};
// inherit Super Class
extendObj(ScrollBackTopSticky, ScrollToLinkClass);

     return ScrollBackTopSticky;
});