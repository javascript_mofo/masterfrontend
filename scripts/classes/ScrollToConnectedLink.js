/**
 * Created by joegoose on 24/06/2015.
 */
define(['classes/ScrollToLinkClass', '../util/extendObj'], function(ScrollToLinkClass, extendObj){


// sub class #1
var ScrollToConnectedLink = function(lin, targ, speed, ease)
{
    this.lin = lin;
    this.targ = targ;
    this.rpm = speed;
    this.ease = ease;
};

// inherit Super Class
extendObj(ScrollToConnectedLink ,ScrollToLinkClass);

        return ScrollToConnectedLink;
});