/**
 * Created by JGasewicz on 03/07/2015.
 */
define(['classes/AddHiddenClass', '../util/extendObj'], function(AddHiddenClass, extendObj)
{

    // create a sub class of AddHiddenClass
    function ConcatenateDobSubClass(d,m,y,nameValue, formName)
    {
        this.day = d;
        this.month = m;
        this.year = y;
        this.nameValue = nameValue;
        this.formName = formName;
    }
extendObj(ConcatenateDobSubClass,AddHiddenClass);
    ConcatenateDobSubClass.prototype = {

        concatDob : function()
        {
            var $dateOfBirth = this.day.val() + " " + this.month.val() + " " + this.year.val();
            var tempDob = new AddHiddenClass($dateOfBirth, $dateOfBirth, this.nameValue, this.formName );
            tempDob.createHiddenFields();
        }

    };
   return ConcatenateDobSubClass;
});