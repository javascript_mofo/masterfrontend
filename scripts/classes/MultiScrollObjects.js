/**
 * Created by JGasewicz on 24/06/2015.
 */

define(['classes/AnimateClass', '../util/extendObj'], function(AnimateClass, extendObj ){
    //  Child of AnimateClass
// Sub Classes #1
    var MultiScrollObjects = function(section, setTime, ef, ht)
    {
        this.section = section;
        this.setTime = setTime;
        this.effect = ef;
        this.height = ht;
    };
// inherit  Super Class
    extendObj(MultiScrollObjects, AnimateClass);

    return MultiScrollObjects;
});
