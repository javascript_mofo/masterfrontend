/**
 * Created by JGasewicz on 24/06/2015.
 */
define(['classes/AnimateClass', '../util/extendObj'], function(AnimateClass,extendObj){
    // Sub Class #2
    var SlideDownText = function(section, setTime, ef){
        this.section = section;
        this.setTime = setTime;
        this.effect = ef;
    };
// inherit Super Class
    extendObj(SlideDownText, AnimateClass);

    return SlideDownText;
});
