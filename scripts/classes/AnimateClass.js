//*******************************************************
//  AnimateClass -- Super Class
//   -- MultiScrollObjects & SlideDownText are sub classes --
//  First Call of jquery here
//*******************************************************
define(['jquery'] ,function($){

    // Super Class
    function AnimateClass(el)
    {
        this.element = el;
    }
    //****************************************
    // Super Class Methods
    //****************************************
    AnimateClass.prototype = {

        // function to slow down animations
        delayAnimate : function(a,i){
            var _this = this;
            if(i){
                a.delay(i)
                    .queue(function(next) {
                        a.addClass(_this.effect);
                        next();
                    });
            }else{
                this.section.delay(this.setTime)
                    .queue(function(next){
                        $(this).addClass(_this.effect);
                        next();
                    });
            }

        },
        // function for multi animations
        multiAnimate : function() {
            var _this = this;
            var i = 0;
            _this.section.each(function(key, value) {
                i = i + _this.setTime;
                _this.delayAnimate($(this),i);
            });
        },
        // test where users screen position is
        userScreenPosition : function(){
            var _this = this;
            $(window).scroll(function() {
                var imagePos = _this.section.offset().top;
                var topOfWindow = $(window).scrollTop();
                if (imagePos < topOfWindow + _this.height) {
                    // call multi animations function
                    _this.multiAnimate();
                }
            });
        }
    };


    return AnimateClass;
});


