define(['jquery'], function($){


    // Super Class
    function PopUpClass(link, page, box)
    {
        this.link = link;
        this.page = page;
        this.box = box;
    }

    // Super Class Methods
    PopUpClass.prototype = {

        // function to show Popup windows
        mainPopFoo : function(){
            var _this = this;
            $(this.link).on("click", function(event){
                event.preventDefault();
                $(".mainMask").fadeIn("fast");
                _this.page.insertAfter($("#closePopUp"));
                _this.page.css("display", "block");
                _this.page.show();
                $(".PopUpBox").addClass('slideDown');


            });
        },

        hidePopUps : function(){
            var  _this = this;
            $(_this.closePop).on("click", function() {
                if(_this.box.is(":visible")) {
                    _this.box.find('div:first').remove();
                    _this.mask.fadeOut("fast");
                    _this.box.removeClass('slideDown');
                    _this.box.css("visibility", "hidden");
                }

            });

        }

    }; // end of methods
    return PopUpClass;
});







