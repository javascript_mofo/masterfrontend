/**
 * Created by JGasewicz on 24/06/2015.
 */
define(["classes/PopUpClass", "../util/extendObj"], function(PopUpClass ,extendObj){
    // Sub Class #2
    var HidePopUp = function(close, box, mask) {
        this.closePop = close;
        this.box= box;
        this.mask = mask;
    };
    // inherit Super Class
    extendObj(HidePopUp, PopUpClass);

    return HidePopUp;

});