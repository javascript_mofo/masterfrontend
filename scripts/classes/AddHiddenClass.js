/**
 * Created by JGasewicz on 02/07/2015.
 */
define(["jquery"], function($){


    function AddHiddenClass(inputId, valOfField, nameValue, formName){
        this.inputId = inputId;
        this.valOfField = valOfField;
        this.nameValue = nameValue;
        this.formName = formName;
        }

    AddHiddenClass.prototype = {

        createHiddenFields : function(){
            var $getInputId = this.inputId;
            $getInputId = this.valOfField;
            var input = $($("<input>"))
                .attr("type", "hidden")
                .attr("name", this.nameValue).val($getInputId);
            $("#" + this.formName).append($(input));
        },

        createTrackingFields : function() {
            var $getInputId = this.inputId;
            $getInputId = this.valOfField;
            var input = $($("<input>"))
                .attr("type", "hidden")
                .attr("name", this.nameValue).val($getInputId);
            $("#" + this.formName).append($(input));
        }


    };


    return AddHiddenClass;
});

