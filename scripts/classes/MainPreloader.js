// Sub Class Of PreLoaderClass
// Sub Class #1

define(['classes/PreLoaderClass', '../util/extendObj'], function(PreloaderClass, extendObj){

    var MainPreloader = function(mainBtn, loaderId, preLoader, myTimeSet, form)
    {
        this.mainBtn = mainBtn;
        this.loaderId = loaderId;
        this.preLoader = preLoader;
        this.myTimeSet = myTimeSet;
        this.form = form;
    };
// inherit Super Class prototype
    extendObj(MainPreloader, PreloaderClass);

    // If Btn clicked
    MainPreloader.prototype.clickPreLoader = function(event)
    {
        var _this = this;
        $( '#' + this.mainBtn).on("click", function(event){
            _this.sendToPreloader();

        });
    };
    // Get Pre Form Values
    MainPreloader.prototype.getPreFormValues = function(f){
        var preFormResult = f.serialize();
        $("#partnerDetialsHidden").remove();
      if($('[name=cover_for]').val() === "single"){

         // alert('single');

          $(".wizard > .steps").css("width", "133%");

          this.sendToPreloader();

      }else if($('[name=cover_for]').val() === "double"){

        //  alert('double');
          $("#partnerDetialsHidden").show();

            $(".wizard > .steps").css("width", "100%");


             this.sendToPreloader();

          // show the extra tabs
      }

    };
    // If already clicked (from validate)
    MainPreloader.prototype.sendToPreloader = function(){

        var _this = this;
        _this.showPreLoader();
        _this.hideElementsWithTime();
        _this.fadeInForm();
    };
    // One Stage submit to Pre Loader
    MainPreloader.prototype.oneStageSendPreLoader = function()
    {
        var _this = this;
        _this.showPreLoader();
        _this.fadeOutOtherElements();

    };
    // For the form submission
    MainPreloader.prototype.sendTothankYouPage = function(){
        var _this = this;
        event.preventDefault();
        _this.showPreLoader();


    };
    // send user to another page
    MainPreloader.prototype.hidePreloader = function()
    {
        this.form.fadeIn();
    };
    // sets a time before sending to page method


    return MainPreloader;
});

