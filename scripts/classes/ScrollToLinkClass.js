/**
 * Created by JGasewicz on 16/06/2015.
 */
    define(['jquery', 'jquery-ui'], function($){


    // Super Class
    function ScrollToLinkClass()
    {

    }

    // Super Class Methods
    ScrollToLinkClass.prototype = {

           //method
        scrollDownToTheLink : function()
        {
            var _this = this;
            $("." + this.lin).on("click", function(event){
                event.preventDefault();
            $('html, body').animate({
             scrollTop: $("#" + _this.targ).offset().top
             }, _this.rpm, _this.ease);

            });

        },
        scrollUpAfterPre : function()
        {
            var _this = this;
            $("#" + this.lin).on("click", function(event){
               // event.preventDefault();
                $('html, body').animate({
                    scrollTop: $("#" + _this.targ).offset().top
                }, _this.rpm, _this.ease);

            });

        },
        scrollBackToTop : function()
        {
            var _this = this;
            $(window).scroll(function(){
                if($(this).scrollTop() > _this.height){
                    $('#toTop').fadeIn();
                }else{
                    $('#toTop').fadeOut();
                }
            });
        }
    };


        return ScrollToLinkClass;


    });







