define(['jquery', 'jquery-ui'], function($){


// Super Class
function PreloaderClass(){}

// main Super Class Methods
PreloaderClass.prototype = {

    showPreLoader : function()
    {
        var _this = this;
        $("#" + this.loaderId).fadeOut(400).remove();
        $("#hero-section-mask").addClass("hero-section-mask");
        $("#" + this.preLoader).fadeIn();
    },

   fadeOutOtherElements : function(){

    $(".hero-text h1:first").slideUp(200);
    $(".hero-text h2:first").slideUp(200);
    },

    fadeOutPreLoader : function(){

        $("#" + this.preLoader).css("display", "none");
        $("#hero-section-mask").removeClass("hero-section-mask");
    },

    hideElementsWithTime : function(){
        var _this = this;
        setTimeout(function(){
            _this.fadeOutPreLoader();
        }, _this.myTimeSet);

    },
    fadeInForm : function(){
        var _this = this;
        if($('#' + _this.preLoader).is(":visible")){
           setTimeout(function(){
               $("#" + _this.form).slideDown(400);
           },_this.myTimeSet);
        }
    }

};
    return PreloaderClass;
});


