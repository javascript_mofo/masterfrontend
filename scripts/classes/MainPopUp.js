/**
 * Created by JGasewicz on 24/06/2015.
 */
define(['classes/PopUpClass', '../util/extendObj'], function(PopUpClass, extendObj){
    // Sub Class #1
    var MainPopUp = function(link, page, box){
        this.link = link;
        this.page = page;
        this.box = box;
    };
// inherit  Super Class
    extendObj(MainPopUp, PopUpClass);

    return MainPopUp;
});
