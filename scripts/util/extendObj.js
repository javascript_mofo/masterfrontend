// Extend the
/**
 * constructor function, whose prototype is the parent object's prototype.
 * Set to child's prototype to the newly created constructor function.
 **/
 define([], function(){
        var extendObj = function(childObj, parentObj)
        {
            var tmpObj = function(){};
            tmpObj.prototype = parentObj.prototype;
            childObj.prototype = new tmpObj();
            childObj.prototype.constructor = childObj;
        };
     return extendObj;
   });
