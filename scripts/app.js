//===========================================================================
// **************************************************************************
//   Main Application File for Website / Forms etc.
//===========================================================================
// Call Dependencies (mainly from the Lib directory)
//****************************************************************************
//  These are sub classes or the AnimateClass
//   -- MultiScrollObjects & SlideDownText sub classes --
//****************************************************************************

    define([
            'classes/MultiScrollObjects',
            'classes/SlideDownText',
            'classes/ScrollToConnectedLink',
            'classes/MainPopUp',
            'classes/HidePopUp',
            'classes/ScrollBackTopSticky',
            'classes/BaseColors'

        ],
        function(MultiScrollObjects,
                 SlideDownText,
                 ScrollToConnectedLink,
                 MainPopUp,
                 HidePopUp,
                 ScrollBackTopSticky,
                 BaseColors
        )
        {
            //**************************************************************
         // Steps Form Methods
         //**************************************************************
         var toTopPreFormSub = new ScrollToConnectedLink("lifeStepsPre","preformFormsteps", 500,'easeOutExpo');
           // toTopPreFormSub.scrollDownToTheLink();
        // Scroll sticky
        var sideScrollSticky = new ScrollBackTopSticky(600);
            sideScrollSticky.scrollBackToTop();

        // Instance of sub class
        var infoSectionEach = new MultiScrollObjects($(".info-section-each"), 200, 'fadeIn',1000);
        var graphBarsAnimate = new MultiScrollObjects($(".graph-bar"), 400, 'pullUp', 1200);
        var graphParaAnimate = new MultiScrollObjects($(".graph-pos p "), 400, 'slideDownSmooth', 1200);

        var slideDownTextOne = new SlideDownText($(".hero-text h1:first"), 400, 'slideDown');
        var slideDownTextTwo = new SlideDownText($(".hero-text h2:first"), 800, 'slideDown');

        // drop down headings
        slideDownTextOne.delayAnimate();
        slideDownTextTwo.delayAnimate();

        // fade in each info section
        if($(".graph-bar").length || $(".info-section-each").length){
            infoSectionEach.userScreenPosition();
            graphBarsAnimate.userScreenPosition();
            graphParaAnimate.userScreenPosition();
        }

        // create instances
        var scrollToFacts = new ScrollToConnectedLink("theFactsLink","theFactsTarget", 1500,'easeOutExpo');
        var scrollToBenefits = new ScrollToConnectedLink("keyBenefitsLink", "keyBenefitsTarget", 1200, 'easeOutExpo');
        var scrollToAboutUs = new ScrollToConnectedLink("aboutUsLink", "aboutUsTarget", 1300, 'easeOutExpo');
        var scrollToApplyNow = new ScrollToConnectedLink("applyNowLink", "applyNowTarget", 2000, 'easeOutExpo');
        // scroll back up to header sticky
        var scrollBackTopOfPage = new ScrollToConnectedLink("toTop", "topOfPage", 1000, 'easeOutExpo');
        var scrollTopLastBtn = new ScrollToConnectedLink("lastBtn","topOfPage", 1000, 'easeOutExpo');
            scrollTopLastBtn.scrollDownToTheLink();
       // var scrollTopOnSubmit = new ScrollToConnectedLink("viewOptionBtn","topOfPage", 1000, 'easeOutExpo');

        // scroll up sticky
        scrollBackTopOfPage.scrollDownToTheLink();

        // scroll down to links
        scrollToFacts.scrollDownToTheLink();
        scrollToBenefits.scrollDownToTheLink();
        scrollToAboutUs.scrollDownToTheLink();
        scrollToApplyNow.scrollDownToTheLink();


            // MainPopUp class instances
            // call the function and link html pages to the footer links
            var firstPopUpOne = new MainPopUp("#linkOne", $("#complaintsProcedure"), $(".PopUpBox") );
            var firstPopUpTwo = new MainPopUp("#linkTwo", $("#privacyPop"), $(".PopUpBox") );
            var firstPopUpThree = new MainPopUp("#linkThree", $("#contactUs"), $(".PopUpBox") );
            var contactUsLink = new MainPopUp("#contactUsLink", $("#contactUs"), $(".PopUpBox") );


            // HidePopUp class instances
            var hidePopUpBox = new HidePopUp($("#closePopUp"),$(".PopUpBox"), $(".mainMask"));
            var screenHidePopUpBox = new HidePopUp( $(".mainMask"),$(".PopUpBox"), $(".mainMask"));


            // header Popups
            contactUsLink.mainPopFoo();
            // Footer Popups
            firstPopUpOne.mainPopFoo();
            firstPopUpTwo.mainPopFoo();
            firstPopUpThree.mainPopFoo();
            // Hide the Popups
            hidePopUpBox.hidePopUps();
            screenHidePopUpBox.hidePopUps();


            // Sub Class # 1 BaseColors
        // Instances
        var btnColorPickerOne = new BaseColors('btn-one');
        var btnColorPickerTwo = new BaseColors('btn-two');

        btnColorPickerOne.swapColorsOnHover();
        btnColorPickerTwo.swapColorsOnHover();

        }    // end of Require.js
    );
