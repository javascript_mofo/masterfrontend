({
    appDir: '../',
    paths: {
        requireLib: 'libs/require'
    },
    baseUrl: 'scripts',
    dir: '../dist',
    modules: [{
        name: 'main',
        include: [
            'app',
            'classes/AddHiddenClass',
            'classes/AnimateClass',
            'classes/BaseColors',
            'classes/ColorPickerClass',
            'classes/ConcatenateDobSubClass',
            'classes/HidePopUp',
            'classes/MainPopUp',
            'classes/MainPreloader',
            'classes/MultiScrollObjects',
            'classes/PopUpClass',
            'classes/PreLoaderClass',
            'classes/ScrollBackTopSticky',
            'classes/ScrollToConnectedLink',
            'classes/ScrollToLinkClass',
            'classes/SlideDownText',
            'validation/main-validation'
        ]

    }],
    mainConfigFile: '../scripts/common.js',
    include: ["requireLib"]

})

